import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'CustomLoginForm.dart';

class ShowDetails extends StatefulWidget {
  var res;

  ShowDetails({this.res});

  @override
  _ShowDetailsState createState() => _ShowDetailsState();
}

class _ShowDetailsState extends State<ShowDetails> {
  var data;
  List count;

//  void statusShow() async{
//    Map count = await getAWBnum(widget.res);
//    for(int i=0;i<count.length;i++)
//  }

  void initState() {
    // TODO: implement initState
    super.initState();
    data = widget.res['data'];
    count = widget.res['data']['statuses'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: Image.asset("images/logo-express.png",
            //height:200.0,
            width: 130.0,
          ),
          backgroundColor: Color(0xffffcd05),
          centerTitle: true,
          actions: <Widget>[
            new FlatButton.icon(onPressed:(){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CustomLoginForm()),
              );} ,
                icon: Icon(Icons.exit_to_app),
                label:Text('Exit') )
          ],
          leading: new Container(),
        ),
      body: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Column(
                    children: <Widget>[
                      new Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 20.0),
                            child: new Text("Package Details",
                                style: new TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xffd4351c),
                                  fontSize: 25.0,
                                  fontFamily: 'nunito',
                                  // height: 3.0,
                                )),
                          ),
                          new Container(
                            //padding: new EdgeInsets.symmetric(vertical: 10.0),
                            child: new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffffcd05),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              "Packages Count:",
                                              style: new TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'nunito',
                                              ),
                                            ),
                                          ],
                                        ))),
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffd4351c ),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              '${data['count']}',
                                              style: new TextStyle(
                                                color:Colors.white,

                                                fontWeight: FontWeight.w400,
                                                fontFamily: 'nunito',
                                              ),
                                            ),
                                          ],
                                        ))),
                              ],
                            ),
                          ),
                          new Container(
                            padding: new EdgeInsets.symmetric(vertical: 10.0),
                            child: new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffffcd05),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              "Payment Method:",
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffd4351c ),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              '${data['payment_type']['title']}',
                                              style: new TextStyle(
                                                  color:Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                              ],
                            ),
                          ),
                          new Container(
                            // padding: new EdgeInsets.symmetric(vertical: 20.0),
                            child: new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffffcd05),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              "Origin:",
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffd4351c ),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              '${data['origin_code']['title']}',
                                              style: new TextStyle(
                                                  color:Colors.white,

                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                              ],
                            ),
                          ),
                          new Container(
                            padding: new EdgeInsets.symmetric(vertical: 10.0),
                            child: new Row(
                              children: <Widget>[
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffffcd05),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              "Destination:",
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                                new Expanded(
                                    child: new Container(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 5.0),
                                        alignment: Alignment.center,
                                        decoration: new BoxDecoration(
                                          borderRadius: new BorderRadius.all(
                                              Radius.circular(10.0)),
                                          color: Color(0xffd4351c ),
                                        ),
                                        child: new Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: <Widget>[
                                            new Text(
                                              '${data['destination_code']['title']}',
                                              style: new TextStyle(
                                                  color:Colors.white,

                                                  fontWeight: FontWeight.w400,
                                                  fontFamily: 'nunito'),
                                            ),
                                          ],
                                        ))),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ),
            //  width: 200,
            new ListView.builder(
              itemCount: count.length,
              reverse: true,
//                      itemCount: count.length ,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return new Stack(
//                          Colo: (index % 2 == 0) ? Colors.red : Colors.green,
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.only(left: 50.0),
                      child: new Card(
                        elevation: 7.0,
                        shape: RoundedRectangleBorder(
                            side: BorderSide(width: 0.2),
                            borderRadius: BorderRadius.circular(20)),
                        margin: new EdgeInsets.all(20.0),
                        child: new Container(
                          child: new Stack(
                            children: <Widget>[
                              new Align(
                                  alignment: Alignment(0.90, -0.80),
                                  child: Text(
                                    '${count[index]['pivot']['date_time']}',
                                    style: TextStyle(
                                        color: (index == count.length - 1)
                                            ? Color(0xffd4351c)
                                            : Colors.white70,
                                        fontFamily: 'nunito'),
                                  )),
                              new Align(
                                  alignment: Alignment(-0.90, -0.35),
                                  child: Text(
                                    '${count[index]['title']}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25.0,
                                        color: (index == count.length - 1)
                                            ? Color(0xffd4351c)
                                            : Colors.white,
                                        fontFamily: 'nunito'),
                                  )),
                              new Align(
                                  alignment: Alignment(-0.90, 0.75),
                                  child: Text(
                                    '${count[index]['pivot']['comment']}',
                                    style: TextStyle(
                                        color: (index == count.length - 1)
                                            ? Color(0xffd4351c)
                                            : Colors.white,
                                        fontFamily: 'nunito'),
                                  )),
                            ],
                          ),
                          decoration: new BoxDecoration(
                            color: (index == count.length - 1)
                                ? Color(0xffffcd05)
                                : Color(0xff333C42),
                            borderRadius:
                            BorderRadius.all(Radius.circular(20.0)),
                            boxShadow: <BoxShadow>[
                              new BoxShadow(
                                color: Color(0xffA1A1A1),
                                blurRadius: 3.0,
                                offset: new Offset(
                                  6.0,
                                  8.0,
                                ),
                              ),
                            ],
                          ),
                          width: double.infinity,
                          height: 100.0,
                        ),
                      ),
                    ),
                    new Positioned(
                      top: 10.0,
                      bottom: 0.0,
                      left: 15.0,
                      child: new Container(
                        decoration: new BoxDecoration(
                          borderRadius:
                          BorderRadius.all(Radius.circular(5.0)),
                          color: Color(0xffd4351c),
                        ),
                        height: double.infinity,
                        width: 15.0,
                        // color: Color(0xffd4351c),
                      ),
                    ),
                    new Positioned(
                      top: 55.0,
                      left: 4.0,
                      child: new Container(
                        height: 40.0,
                        width: 40.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: new Container(
                          margin: new EdgeInsets.all(5.0),
                          height: 30.0,
                          width: 30.0,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xffffcd05)),
                        ),
                      ),
                    )
                  ],
                );
              },
            ),
          ]
      ),
    );
  }
}
