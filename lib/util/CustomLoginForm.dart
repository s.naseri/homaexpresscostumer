import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:homaexpresscostumer/awb_number/awb_number.dart' as awb;
import 'package:progress_indicator_button/progress_button.dart';

import 'package:homaexpresscostumer/util/ShowDetailsPage.dart';

class CustomLoginForm extends StatefulWidget {
  @override
  _CustomLoginFormState createState() => _CustomLoginFormState();
}


class _CustomLoginFormState extends State<CustomLoginForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String trakingCode = "";
  TextEditingController _textController = new TextEditingController();
  void ShowStuff() async {
    Map data = await getAWBnum(awb.awbNumber);
    print(data.toString());
  }

  @override
  final _formkey = GlobalKey<FormState>();
  var myController;

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          new Image.asset(
            "images/backAppHomaexpress.png",
            height: 2000.0,
            width: 700.0,
            fit: BoxFit.fill,
          ),
          new ListView(
            children: <Widget>[
              Form(
                key: _formkey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30,vertical: 180),
                  child: new Container(
                    decoration: new BoxDecoration(
                        color: Color(0x99d01818),
                        borderRadius: new BorderRadius.all(Radius.circular(5))
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: new Center(
                            child: new Text(
                              "TRACK & TRACE",
                              style: TextStyle(
                                  fontSize: 30.0,
                                  color: Colors.white,
                                  fontFamily: 'sego',
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Padding(
                            padding:
                            const EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 10.0)),
                        Padding(
                          padding:
                          const EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                          child: new TextFormField(
                            controller: _textController,
                            decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,

//                              focusedBorder: UnderlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Color(0xffd01818),
//                                ),
//                              ),
                                errorStyle: new TextStyle(
                                    color: Colors.black,
                                    fontSize: 15
                                ),
                                errorBorder: OutlineInputBorder(

                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.orange),
                                ),
//
                                hintText: "Tracking Code",
                                hintStyle: TextStyle(
                                  color: Color(0x55d01818),
                                  fontFamily: 'sego',
                                ),
                                border: OutlineInputBorder(
                                    gapPadding: 3.0,
                                    borderRadius: BorderRadius.circular(10.0))
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Please Enter Your Tracking Code";
                              } else {
                                trakingCode = value;
                                _textController.clear();
                                return null;
                              }

                            },

                          ),
                        ),
                        new Center(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 15),
                                child: new Container(
                                  width: 150,
                                  height: 50,
                                  child: ProgressButton(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    color: Colors.red[900],
                                    strokeWidth: 2,
                                    child: Text(
                                      "search",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                      ),
                                    ),
                                    onPressed: (AnimationController controller) async{
                                      controller.forward();
                                      myController=controller;
                                      if(_formkey.currentState.validate()){
                                         var score = await Future.delayed(
                                            const Duration(milliseconds: 30));
                                        var responseBody= await getAWBnum(trakingCode);
                                        await _progressResponseBody(responseBody);
                                      }else{
                                        controller.reverse();
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  _progressResponseBody(Map responseBody) {
    var status=responseBody['status'];
    _scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(status)));
    print(status);
    if(status=='ok'){
      Navigator.push(context, MaterialPageRoute(builder: (context){
        return new ShowDetails(res: responseBody);
      }));
    }else if(status=='error'&& trakingCode==null){

//      var score = Future.delayed(
//          const Duration(milliseconds: 30));
      myController.reverse();
    }
  }
}

Future<Map> getAWBnum(String awbNumber) async {
  print(awbNumber);
//  awbNumber="1100000044";

  String apiUrl =
      'http://homaexpressco.ir/api/trackByAWB?awbNumber=${awbNumber}';
  http.Response response = await http.get(apiUrl);
//  print(json.decode(response.body));
  return json.decode(response.body);
}
