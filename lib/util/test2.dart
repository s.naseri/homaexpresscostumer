import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';

class ShowDetails extends StatefulWidget {
//  var res;
//  ShowDetails({this.res});
  @override
  _ShowDetailsState createState() => _ShowDetailsState();
}

class _ShowDetailsState extends State<ShowDetails> {
//  var data;
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    data = widget.res['data'];
//  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new Container(
          padding: EdgeInsets.symmetric(vertical: 20.0),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height / 3,
                child: ListView(children: <Widget>[
                  new Container(
                      padding: EdgeInsets.symmetric(vertical: 20.0),
                      child: Column(
                        children: <Widget>[
                          new Column(
                            children: <Widget>[
                              new Text("Package Details",
                                  style: new TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xffd01818),
                                    fontSize: 25.0,
                                  )),
                              new Container(
                                //padding: new EdgeInsets.symmetric(vertical: 0.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffffe88e),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  "Packages Count:",
                                                  style: new TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ],
                                            ))),
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffff25b43),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  "istanbul (IST)",
                                                  style: new TextStyle(
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ],
                                            ))),
                                  ],
                                ),
                              ),
                              new Container(
                                padding: new EdgeInsets.symmetric(vertical: 20.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffffe88e),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  "Payment Method:",
                                                  style: new TextStyle(
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ))),
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffff25b43),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  '2',
                                                  style: new TextStyle(
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ))),
                                  ],
                                ),
                              ),
                              new Container(
                                // padding: new EdgeInsets.symmetric(vertical: 20.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffffe88e),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  "Origin:",
                                                  style: new TextStyle(
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ))),
                                    new Expanded(
                                        child: new Container(
                                            padding:
                                            EdgeInsets.symmetric(vertical: 5.0),
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(0xffff25b43),
                                            ),
                                            child: new Row(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                              MainAxisAlignment.center,
                                              children: <Widget>[
                                                new Text(
                                                  "2",
                                                  style: new TextStyle(
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ))),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      )),
                ]),
              ),
              Container(
                height: MediaQuery.of(context).size.height - MediaQuery.of(context).size.height / 3,
                width: 200,
                child: new Row(
                  children: <Widget>[
                    Expanded(
                      child: new ListView.builder(
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return new Stack(
                            children: <Widget>[
                              new Padding(
                                padding: const EdgeInsets.only(left: 50.0),
                                child: new Card(
                                  margin: new EdgeInsets.all(20.0),
                                  child: new Container(
                                    width: double.infinity,
                                    height: 200.0,
                                    color: Colors.green,
                                  ),
                                ),
                              ),
                              new Positioned(
                                top: 0.0,
                                bottom: 0.0,
                                left: 35.0,
                                child: new Container(
                                  height: double.infinity,
                                  width: 1.0,
                                  color: Colors.blue,
                                ),
                              ),
                              new Positioned(
                                top: 100.0,
                                left: 15.0,
                                child: new Container(
                                  height: 40.0,
                                  width: 40.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                  ),
                                  child: new Container(
                                    margin: new EdgeInsets.all(5.0),
                                    height: 30.0,
                                    width: 30.0,
                                    decoration: new BoxDecoration(
                                        shape: BoxShape.circle, color: Colors.red),
                                  ),
                                ),
                              )
                            ],
                          );
                        },
                        itemCount: 5,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
