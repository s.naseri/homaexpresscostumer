
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homaexpresscostumer/util/CustomLoginForm.dart';
import 'package:homaexpresscostumer/util/ShowDetailsPage.dart';
import 'package:homaexpresscostumer/util/testtimeline.dart';



void main() =>runApp(login());

class login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = "login";
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        title: appTitle,
        home : Scaffold(
          appBar: new AppBar(
            title: Image.asset("images/logo-express.png",
            //height:200.0,
            width: 130.0,
    ),
            backgroundColor: Color(0xffffcd05),
            centerTitle: true,

          ),
          body: CustomLoginForm(),
        )
    );
  }
}
